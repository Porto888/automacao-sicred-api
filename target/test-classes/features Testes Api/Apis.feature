
#Author: guilhermepstk@hotmail.com
Feature: Prova admissional DBC/Sicred.

  @tag1
  Scenario: Consultanto restri��o poss�vel no CPF, para libera��o de cr�dito. (CPF com restri��o)
  
 
    Given Joana da Silva, est� com deificuldades financeiras para solucionar o problema Joana decidiu fazer um empr�stimo no Banco Sicred,
    e para isso a mesma informou o seu CPF, no site da Sicred para consultar a disponibilidade do servi�o, e assim conseguir regularizar seu
    or�amento.
  
    When Ap�s digitar o seu cpf.
   
    Then Joana descobriu que possui uma restri��o, e n�o poder� contar com  servi�o de cr�dito. Pois recebeu a seguinte menssagem:
    ("O CPF 99999999999 possui restri��o. ")
    
    
  @tag2
  Scenario: Consultanto restri��o poss�vel no CPF, para libera��o de cr�dito. (CPF sem restri��o)
  
 
    Given Magno, est� com deificuldades financeiras para solucionar o problema Joana decidiu fazer um empr�stimo no Banco Sicred,
    e para isso a mesma informou o seu CPF, no site da Sicred para consultar a disponibilidade do servi�o, e assim conseguir regularizar seu
    or�amento.
  
    When Ao digitar seu cpf.
   
    Then Magno, foi direcionado para a p�gina de cadastro.


  @tag3
  Scenario Outline: Prenchemento de ficha cadastraral. (Sucesso!!!)
  
  
    Given Ap�s a verifica��o de restri��o no cpf, Carlos Alfredo foi direcionado para a p�gina de cadastro
    
    When Carlos realizou o preenchimento de todos os campos solicitados.
    
    Then Em seguida Caarlos pode visualizar em sua tela os seus dados cadastrados.
    
 
  @Tag4
  Scenario Outline: Prenchemento de ficha cadastraral. (Error.)
  
  
    Given Ap�s a verifica��o de restri��o no cpf, Maria Clara foi direcionado para a p�gina de cadastro
    
    When Maria Clara realizou o preenchimento de todos os campos solicitados, por�m ela j� hahia preenchido a ficha cadastral
    dias antes.
    
    Then Desta forma Maria recebeu a mensagem de: (" CPF j� existente ").
    
    
  @Tag5
  Scenario Outline: Alterar uma simula��o. 
  
  
    Given Ap�s preencher a ficha cadastral no site do Banco Sicred.
    
    When Jo�o notou que, deigitou o seu cep incorretamente.
    
    Then Logo Jo�o, teve que preencher novamente o cadastro, e colocar cep correto.
    
    
  @Tag6
  Scenario Outline: Consultando simula��es(Lista cheia).
  
  
    Given Afim de saber quantas pessoas se interessam, pelo servi�o de cr�dito online.
    
    When O Gerente, do departamento, acessou o seu sistema e clicando no bot�o de consulta.
    
    Then Recebeu uma lista com todas as simula��es de cr�dito cadastradas.
    
      
  @Tag7
  Scenario Outline: Consultando simula��es(Lista vazia).
  
  
    Given Afim de saber quantas pessoas se interessam, pelo servi�o de cr�dito online.
    
    When O Gerente, do departamento, acessou o seu sistema e clicando no bot�o de consulta.
    
    Then Recebeu uma lista vazia pois n�o haviam cadastro no per�odo pesquisado.  
    
  
  @Tag8
  Scenario Outline: Consultando uma simula��o.
  
  
    Given Lurdes est� precisando de um empr�stimo, mas n�o tem certeza se j� realizou um cadastro no sistema Sicred.
    
    When Lurdes acessou o site do banco Sicred, e atrav�s da po��o consultar simula��o.
    
    Then P�de verificar que, j� tem um cadastro no sistema Sicred.
    
    
  @Tag9
  Scenario Outline: Deletando uma Simula��o.
  
  
    Given Quando quero remover uma simula��o j� cadastrada.
    
    When Eu acesso o seu sistema e clico no bot�o de consulta, caso eu tenha algum cadastro ser� gerado o meu id.
    
    Then Em posse do id, devo apenas digita-l� no campo solicitado e clicar no bot�o delete.
    
    
    
    

