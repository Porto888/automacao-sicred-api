package validacaoDeCpf;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.event.ContainerListener;
import java.nio.file.Path;
import java.util.ArrayList;

import org.hamcrest.Matcher;
import org.junit.jupiter.api.Test;

import com.sun.xml.bind.v2.schemagen.xmlschema.List;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.Data;

@Data
public class ProvaSicredRestricaoApi {
	private String Id;
	private String authorization;

	@Test
	public void RestricaoApi() {

		Response response =

				given().contentType("aplication/json").when()
						.get("http://localhost:8080/api/v1/restricoes/26276298085");

		response.then().log().all();

		String mensagem = response.jsonPath().get("mensagem");
		System.out.println("Mensagem retornada=> " + mensagem);

	}

	@Test
	public void simulacaoCadastroApi() {
		String cpfTest = ("97093236014, 60094146012, 84809766080, 62648716050, 26276298085, 01317496094, 55856777050, 19626829001, 24094592008, 58063164083");

		Response response =

				given().contentType("application/json")
				.body("{\"nome\": \"Chico\", \"cpf\": 60094146012, \"email\": \"email@email.com\", \"valor\": 28000, \"parcelas\": 4,\"seguro\": true}")

						.when().post("http://localhost:8080/api/v1/simulacoes");

		(response).then()

				.log().all();

		String mensagem = response.jsonPath().get("mensagem");
		System.out.println("mensagem" + mensagem);
		
		String Error= response.jsonPath().get("Error");
		System.out.println("Error" + Error);

	}

	@Test
	public void AlterarSimulacao() {

		given().contentType("application/json").body(
				"{\"nome\": \"Z� de Antonia\", \"cpf\": \60094146012, \"email\": \"email@email.com\", \"valor\": 15000, \"parcelas\": 5,"
						+ "\"seguro\": true}")

				.when().put("http://localhost:8080/api/v1/simulacoes/cpf")

				.then()

				.log().all();

	}

	@Test
	public void TodasAsSimulacoesApi() {

		Response response =

				given().contentType("aplication/json")

						.when().get("http://localhost:8080/api/v1/simulacoes");

		response.then().log().all().statusCode(200);

	}

	@Test
	public void ConsultandoUmaSimulacao() {

		Response response =

				given().contentType("aplication/json")

						.when().get("http://localhost:8080/api/v1/simulacoes/60094146012");

		response.then().log().all().statusCode(200);

	}

	@Test
	public void ApagarCadastro() {

		Response response =

				given().contentType("aplication/json").when().delete("http://localhost:8080/api/v1/simulacoes/78");

		response.then().log().all().statusCode(200);
		
		
		System.out.println("Que o sucesso esteja conosco!!!");

	}

}
